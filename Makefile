#DEBUG = 1

CC=gcc

CFLAGS += -Wall -ggdb3 
ifdef DEBUG
CFLAGS+= -g -O0
else
CFLAGS+= -O2
endif
CFLAGS += $(shell pkg-config --cflags glib-2.0)
LIBS    = $(shell pkg-config --libs glib-2.0 --libs gthread-2.0) -lhiredis
OBJS+= main.o splay.o parda.o parda_print.o narray.o process_args.o pardus_redis.o
HEADERS= splay.h parda.h narray.h process_args.h pardus_redis.h


SOURCES=$(subst .o,.c, $(OBJS) )
EXE=parda
.PHONY: all clean gnuplots run
all: $(EXE)

$(EXE): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $+ $(LIBS)
	cp -f parda ../ls
$(OBJS):$(HEADERS) makefile
%.d: %.c
	set -e; rm -f $@; \
	$(CC) -M $(CPPFLAGS) $< > $@.$$$$; \
        sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
        rm -f $@.$$$$
include $(sources:.c=.d)
clean:
	rm -f $(EXE) *.o 
run:
