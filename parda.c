#include "parda.h"
#include "narray.h"
#include "pardus_redis.h"


/*
  An implementation of Parda, a fast parallel algorithm
to compute accurate reuse distances by analysis of reference
traces.
  Qingpeng Niu
*/



int nbuckets  = DEFAULT_NBUCKETS;
void iterator(gpointer key, gpointer value, gpointer ekt) {
  HKEY temp;
  strcpy(temp, key);
  narray_append_val(((end_keytime_t*)ekt)->gkeys, temp);
  narray_append_val(((end_keytime_t*)ekt)->gtimes, value);
}


program_data_t parda_init() {
  program_data_t pdt;
  GHashTable *gh;
  narray_t* ga = narray_new(sizeof(HKEY), 1000);
  Tree* root;
  unsigned int *histogram;
  histogram = malloc(sizeof(unsigned int) * (nbuckets+2));
  gh = g_hash_table_new_full(g_str_hash, compare_strings, free, free);
  root = NULL;
  memset(histogram, 0, (nbuckets + 2) * sizeof(unsigned int));
  pdt.ga = ga;
  pdt.gh = gh;
  pdt.root = root;
  pdt.histogram = histogram;
  return pdt;
}

gboolean compare_strings(gconstpointer a, gconstpointer b) {
  if (strcmp(a,b) == 0)
    return TRUE;
  else
    return FALSE;
}

void parda_process(char* input, T tim, program_data_t* pdt) {
  GHashTable *gh = pdt->gh;
  Tree* root = pdt->root;
  narray_t* ga = pdt->ga;
  unsigned int *histogram = pdt->histogram;
  int distance;
  T *lookup;
  lookup = g_hash_table_lookup(gh, input);
  if (lookup == NULL) {
    char* data = strdup(input);
    root = insert(tim, root);
    T *p_data;

    narray_append_val(ga, input);
    if (!(p_data = (T*)malloc(sizeof(T)))) exit(1);
    *p_data = tim;
    g_hash_table_insert(gh, data, p_data);  // Store pointer to list element
  }

  // Hit: We've seen this data before
  else {
    root = insert((*lookup), root);
    distance = node_size(root->right);
    root = delete(*lookup, root);
    root = insert(tim, root);
    int *p_data;
    if (!(p_data = (int*)malloc(sizeof(int)))) exit(1);
    *p_data = tim;
    g_hash_table_replace(gh, strdup(input), p_data);

    // Is distance greater than the largest bucket
    if (distance > nbuckets)
      histogram[B_OVFL]++;
    else
      histogram[distance]++;
  }
  pdt->root = root;
}

processor_info_t parda_get_processor_info(int pid, int psize, long sum) {
  processor_info_t pit;
  pit.tstart = parda_low(pid, psize, sum);
  pit.tend = parda_high(pid, psize, sum);
  pit.tlen = parda_size(pid, psize, sum);
  pit.sum = sum;
  pit.pid = pid;
  pit.psize = psize;
  return pit;
}



double rtclock() {
  struct timezone Tzp;
  struct timeval Tp;
  int stat;
  stat = gettimeofday (&Tp, &Tzp);
  if (stat != 0) printf("Error return from gettimeofday: %d",stat);
  return(Tp.tv_sec + Tp.tv_usec*1.0e-6);
}



program_data_t* global_pdt = NULL;
void sig_handler(int signo)
{
  if (signo == SIGUSR1) {
    printf("received SIGUSR1\n");
    if (global_pdt != NULL) {
      parda_print_histogram(global_pdt->histogram);
    } else {
      printf("NOT printing histogram, pdt is NULL\n");
    }
  } else if (signo == SIGUSR2) {
    printf("received SIGUSR2\n");
    if (global_pdt != NULL) {
      printf("writing histogram to Redis\n");
      pardus_write_histogram_to_redis(global_pdt->histogram);
    } else {
      printf("NOT writing histogram, pdt is NULL\n");
    }
  } else if (signo == SIGINT || signo == SIGTERM) {
    printf("received SIGINT or SIGTERM\n");
    printf("unregistering pardus\n");
    redis_unregister_pardus();
    printf("exiting\n");
    exit(0);
  } else if (signo == SIGHUP) {
    printf("received SIGHUP\n");
    printf("TODO: Write code to reset the splay tree\n");
    //program_data_t pdt_c = parda_init();
  }
}



void classical_tree_based_stackdist(char* inputFileName) {
  if (signal(SIGUSR1, sig_handler) == SIG_ERR) {
    printf("\ncan't catch SIGUSR1\n");
  }
  if (signal(SIGUSR2, sig_handler) == SIG_ERR) {
    printf("\ncan't catch SIGUSR1\n");
  }
  if (signal(SIGINT, sig_handler) == SIG_ERR) {
    printf("\ncan't catch SIGINT\n");
  }
  if (signal(SIGTERM, sig_handler) == SIG_ERR) {
    printf("\ncan't catch SIGTERM\n");
  }
  redis_register_pardus();
#ifdef enable_timing
  double ts, te, t_init, t_input, t_print, t_free;
  ts = rtclock();
#endif
  PTIME(te = rtclock();)
    PTIME(t_init = te - ts;)
    program_data_t pdt_c = parda_init();
    global_pdt = &pdt_c;
    parda_input_with_filename(inputFileName, &pdt_c, 0);
  program_data_t *pdt = &pdt_c;
  pdt->histogram[B_INF] += narray_get_len(pdt->ga);
  PTIME(ts = rtclock();)
    PTIME(t_input = ts - te;)
    parda_print_histogram(pdt->histogram);
  PTIME(te = rtclock();)
    PTIME(t_print = te - ts;)
    parda_free(pdt);
  PTIME(ts = rtclock();)
    PTIME(t_free = ts - te;)
#ifdef enable_timing
    printf("seq\n");
  printf("init time is %lf\n", t_init);
  printf("input time is %lf\n", t_input);
  printf("print time is %lf\n", t_print);
  printf("free time is %lf\n", t_free);
#endif
}

void parda_input_with_filename(char* inFileName, program_data_t* pdt, long begin) {
  printf("inFileName=[%s]\n", inFileName);
  printf("strncmp=%d\n", strncmp(inFileName, "-", 1));
  DEBUG(printf("enter parda_input < %s from %ld to %ld\n", inFileName, begin, end);)
  FILE* fp;
  fp = fopen(inFileName, "r");
  if (strncmp(inFileName, "-", 1) == 0) {
    printf("using stdin\n");
    fp = stdin;
  } else {
    printf("NOT using stdin\n");
    fp = fopen(inFileName, "rb");
  }
  parda_input_with_textfilepointer(fp, pdt, begin);
  fclose(fp);
}


void parda_input_with_textfilepointer(FILE* fp, program_data_t* pdt, long begin) {
  HKEY input;
  long i = begin;
  while (!feof(fp)) {
    fscanf(fp, "%s", input);
    DEBUG(printf("%s %d\n", input, i);)
    process_one_access(input, pdt, i);
    i++;
  }
}


void parda_free(program_data_t* pdt) {
  narray_free(pdt->ga);
  //g_hash_table_foreach(pdt->gh, free_key_value, NULL);
  g_hash_table_destroy(pdt->gh);
  free(pdt->histogram);
  freetree(pdt->root);
}

void pardus_write_histogram_to_redis(const unsigned* histogram) {
  int last_bucket;
  int i;
  unsigned long long sum = 0;  // For normalizing
  unsigned long long cum = 0;  // Cumulative output

  redis_clear_stats();
  // Find the last non-zero bucket
  last_bucket = nbuckets-1;
  while (histogram[last_bucket] == 0)
    last_bucket--;

  for (i = 0; i <= last_bucket; i++)
    sum += histogram[i];
  sum += histogram[B_OVFL];
  sum += histogram[B_INF];


  for (i = 0; i <= last_bucket; i++) {
    cum += histogram[i];
    redis_rpush_stats(cum / (double) sum);
  }

  cum += histogram[B_OVFL];
  printf("#OVFL \t%9u\t%0.8f\t%9llu\t%0.8lf\n", histogram[B_OVFL], histogram[B_OVFL]/(double)sum, cum, cum/(double)sum);
  cum += histogram[B_INF];
  printf("#INF  \t%9u\t%0.8f\t%9llu\t%0.8lf\n", histogram[B_INF], histogram[B_INF]/(double)sum, cum, cum/(double)sum);
}

