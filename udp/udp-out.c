/* read stuff from udp (on given port) and write it to stdout */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <sys/socket.h>
#include <netinet/in.h>

int main(int argc, char *argv[])
{
    int sock, n = 0;
    struct sockaddr_in servaddr;
    char message[128];

    if (argc < 2) {
        printf("Usage: udp-out <listen port>\n");
        return 1;
    }
    int port = atoi(argv[1]);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    bzero(&servaddr, sizeof(servaddr));

    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
    servaddr.sin_port=htons(port);

    bind(sock, (struct sockaddr *)&servaddr, sizeof(servaddr));

    while (1) {
        n = recv(sock, message, sizeof(message), 0);
        write(1, message, n);
    }
}
