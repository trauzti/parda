This is a tiny little UDP listener used to pipe messages from a
redis server with keyhit-sampling enabled.  Example usage:

```
~/parda $ redis-cli config set keyhit-sampling 1.0
~/parda $ udp/udp-out 12345 | ./parda
~/parda $ redis-cli
127.0.0.1:6379> get test_key
```

Now udp-out will have emitted the string "test_key\n" on stdout and
parda will have read it
