Reuse distance is a well established approach to characterizing data cache locality based on the stack histogram model. 
This analysis so far has been restricted to ofﬂine use due to the high cost, often several orders of magnitude larger than the execution time of the analyzed code. Parda is the ﬁrst parallel algorithm to compute accurate reuse distances by analysis of memory address traces. The algorithm uses a tunable parameter that enables faster analysis when the maximum needed reuse distance is limited by a cache size upper bound. 

$ sudo apt-get install glib
$ DEBUG = 1
$ make

====Execution instructions====

$ ./parda --help to see how to run with different flags and run with sequential algorithm. 

Execution arguments
Read from a file:
--input=filename
Read from stdin:
--input=- :

$ ./parda.x --input=normal_137979.trace  


Parda is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Parda is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Author: Qingpeng Niu

Contact: niuqingpeng at gmail.com

Documententation

Related publications:

PARDA: A Fast Parallel Reuse Distance Analysis Algorithm.
Qingpeng Niu, James Dinan, Qingda Lu and P. Sadayappan.
IEEE IPDPS (IPDPS'12), May 2012, Shanghai, China.
