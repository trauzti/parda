import redis
import time


class PardusCoordinator(object):
    CLIENT_LIST_KEY = "PARDUS_CLIENTS"
    CLIENT_KEY = "PARDUS_CLIENT:{}".format
    STATS_KEY = "PARDUS_STATS:{}".format

    def __init__(self, redis_client):
        self.redis = redis_client
        self.init()

    def init(self):
        self.pardi = {}
        try:
            clients = self.redis.lrange(self.CLIENT_LIST_KEY, 0, -1)
            for client_idx in clients:
                self.pardi[client_idx] = self.get_client(client_idx)
        except redis.exceptions.ResponseError:
            print "Client list key: {} not found, retrying..".format(self.CLIENT_LIST_KEY)
            time.sleep(10)
            return self.init()

    def get_client(self, index):
        return self.redis.hgetall(self.CLIENT_KEY(index))

    def get_stats(self, index):
        return self.redis.lrange(self.STATS_KEY(index))

    def get_memory(self, host, port=6370, human=False):
        r = redis.Redis(host, int(port))
        maxmemory = r.config_get('maxmemory').get('maxmemory', 0)
        current_memory = r.info().get('used_memory', None)
        if maxmemory == '0' and human is True:
            maxmemory = "Inf"
        return current_memory, maxmemory

    def run(self):
        print "PARDUS Coordinator running"
        print " {} clients connected".format(len(self.pardi))
        for client in sorted(self.pardi.keys()):
            data = self.pardi[client]
            print "------------------------------------"
            print " client:   {}".format(client)

            if data == {}:  # this pardus has not registered anything?
                print " .. no more data!"
                continue

            host = data['host'].split(":")
            print " host:     {}".format(data['host'])
            print " pid:      {}".format(data['pid'])
            print " keycount: {}".format(data['keycount'])
            print " memory:   {} / {}".format(*self.get_memory(*host, human=True))


if __name__ == "__main__":
    config_redis = redis.Redis('localhost', 6390)
    boss = PardusCoordinator(config_redis)
    i = 0
    while True:
        boss.run()
        print "========================================================="
        time.sleep(15)
        i += 1
        if i % 5 == 0:
            boss.init()  # reload clients every 5 rounds
