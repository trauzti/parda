#include "parda.h"
#include "process_args.h"

int main(int argc, char **argv)
{
    process_args(argc,argv);
    DEBUG(printf("This is seq stackdist\n");)
    classical_tree_based_stackdist(inputFileName);
    return 0;
}
