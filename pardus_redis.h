#ifndef PARDUS_REDIS_H
#define PARDUS_REDIS_H

#include <hiredis/hiredis.h>

redisContext *getRedisContext();
void redis_register_pardus();
void redis_unregister_pardus();
int getPardusClientId();
void redis_clear_stats();
void redis_rpush_stats(double value);


#endif //PARDUS_REDIS_H
