#include <unistd.h>
#include "pardus_redis.h"

redisContext *conn = NULL;
int pardusClientId = -1;
char redis_stats_host[] = "127.0.0.1";
int redis_stats_port = 6390;


char redis_main_host[] = "127.0.0.1";
int redis_main_port = 6379;

char pardus_stats[] = "PARDUS_STATS";
char pardus_client[] = "PARDUS_CLIENT";
char pardus_clients[] = "PARDUS_CLIENTS";
char pardus_client_count[] = "PARDUS_CLIENT_COUNT";

redisContext *getRedisContext() {
    if (conn == NULL) {
        printf("Making a new redis connection!\n");
        conn = redisConnect(redis_stats_host, redis_stats_port);
    }
    if (conn != NULL && conn->err) {
        printf("Error connecting to redis: %s\n", conn->errstr);
        redisFree(conn);
        conn = NULL;
        return NULL;
    }
    return conn;
}

void redis_register_pardus() {
    redisReply *reply;

    redisContext *conn = getRedisContext();
    if (conn == NULL) {
      return;
    }
    reply = redisCommand(conn, "INCR %s", pardus_client_count);
    pardusClientId = reply->integer;
    redisCommand(conn, "RPUSH %s %d", pardus_clients, pardusClientId);
    int pid = getpid(); // cast from pid_t to int
    redisCommand(conn, "HSET %s:%d pid %d",pardus_client, pardusClientId, pid);
    redisCommand(conn, "HSET %s:%d keycount 0",pardus_client, pardusClientId);
    redisCommand(conn, "HSET %s:%d host %s:%d", pardus_client, pardusClientId, redis_main_host, redis_main_port);

    return;
}

void redis_pardus_update_keycount(int keycount) {
    redisContext *conn = getRedisContext();
    if (conn == NULL) {
      return;
    }
    if (pardusClientId == -1) {
      printf("Could not update keycount, client id is -1\n");
      return;
    }
    redisCommand(conn, "HSET %s:%d keycount %d",pardus_client, pardusClientId, keycount);
}

void redis_unregister_pardus() {
    int cid = pardusClientId;
    if (cid == -1) {
      printf("Pardus client id is not initialized\n");
      printf("Cannot unregister pardus\n");
      return;
    }
    int count = 0; // remove all elements with this value
    redisCommand(conn, "LREM %s %d %d", pardus_clients, count, cid);
    redis_clear_stats();
}

int getPardusClientId() {
  return pardusClientId;
}

void redis_clear_stats() {
    redisContext *conn = getRedisContext();
    if (conn == NULL) {
      return;
    }
    int cid = getPardusClientId();
    if (cid == -1) {
      printf("Pardus client id is not initialized\n");
      printf("Cannot clear stats\n");
      return;
    }
    redisCommand(conn, "DEL %s:%d", pardus_client, cid);
}

void redis_rpush_stats(double value) {
    redisContext *conn = getRedisContext();
    if (conn == NULL) {
      return;
    }
    int cid = getPardusClientId();
    if (cid == -1) {
      printf("Pardus client id is not initialized\n");
      printf("Cannot unregister pardus\n");
      return;
    }
    redisCommand(conn, "RPUSH %s:%d %lf", pardus_stats, cid, value);
}
